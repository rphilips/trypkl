// Code generated from Pkl module `MApp`. DO NOT EDIT.
package config

import (
	"trypkl/config/club"
	"trypkl/config/match"
)

type Config struct {
	Clubs []*club.Club `pkl:"clubs"`

	Matches []*match.Match `pkl:"matches"`
}
