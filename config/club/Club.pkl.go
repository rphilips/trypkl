// Code generated from Pkl module `MClub`. DO NOT EDIT.
package club

type Club struct {
	Code string `pkl:"code"`

	Name string `pkl:"name"`

	Address string `pkl:"address"`
}
