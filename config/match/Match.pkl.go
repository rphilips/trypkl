// Code generated from Pkl module `MMatch`. DO NOT EDIT.
package match

type Match struct {
	Date string `pkl:"date"`

	Home string `pkl:"home"`

	Visitor string `pkl:"visitor"`
}
