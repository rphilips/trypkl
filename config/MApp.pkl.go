// Code generated from Pkl module `MApp`. DO NOT EDIT.
package config

import (
	"context"

	"github.com/apple/pkl-go/pkl"
)

type MApp struct {
	Config *Config `pkl:"config"`
}

// LoadFromPath loads the pkl module at the given path and evaluates it into a MApp
func LoadFromPath(ctx context.Context, path string) (ret *MApp, err error) {
	evaluator, err := pkl.NewEvaluator(ctx, pkl.PreconfiguredOptions)
	if err != nil {
		return nil, err
	}
	defer func() {
		cerr := evaluator.Close()
		if err == nil {
			err = cerr
		}
	}()
	ret, err = Load(ctx, evaluator, pkl.FileSource(path))
	return ret, err
}

// Load loads the pkl module at the given source and evaluates it with the given evaluator into a MApp
func Load(ctx context.Context, evaluator pkl.Evaluator, source *pkl.ModuleSource) (*MApp, error) {
	var ret MApp
	if err := evaluator.EvaluateModule(ctx, source, &ret); err != nil {
		return nil, err
	}
	return &ret, nil
}
