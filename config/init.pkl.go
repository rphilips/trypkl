// Code generated from Pkl module `MApp`. DO NOT EDIT.
package config

import "github.com/apple/pkl-go/pkl"

func init() {
	pkl.RegisterMapping("MApp", MApp{})
	pkl.RegisterMapping("MApp#App", Config{})
}
