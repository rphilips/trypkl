/*
Copyright © 2024 Richard Philips <richard.philips@gmail.com>
*/
package main

import (
	"cmp"
	"context"
	"fmt"
	"slices"
	"strings"
	"time"

	"trypkl/config"
	"trypkl/config/club"
	"trypkl/config/match"
)

var Config *config.Config

func main() {

	today := time.Now().Format(time.DateOnly)
	baseClub := "430"
	matches := make([]*match.Match, 0)
	for _, match := range Config.Matches {
		if match.Date < today {
			continue
		}
		if match.Home != baseClub && match.Visitor != baseClub {
			continue
		}
		matches = append(matches, match)
	}
	slices.SortFunc(matches, func(a, b *match.Match) int {
		return cmp.Compare(a.Date, b.Date)
	})

	for i, match := range matches {
		if i != 0 {
			fmt.Println()
		}
		fmt.Println(match.Date)
		home := match.Home
		visitor := match.Visitor
		other := home
		if home == baseClub {
			other = visitor
			fmt.Println("    we play at home against:")
		}
		adv, err := adversary(other, home != baseClub, "    ")
		if err != nil {
			panic(err)
		}
		fmt.Println(adv)
	}

}

func init() {
	mconfig, err := config.LoadFromPath(context.Background(), "/home/rphilips/.config/trypkl/config.pkl")
	if err != nil {
		panic(err)
	}
	Config = mconfig.Config
}

func adversary(code string, address bool, indent string) (adv string, err error) {
	name := ""
	var club *club.Club
	for _, c := range Config.Clubs {
		if c.Code == code {
			club = c
			break
		}
	}
	if club == nil {
		return "", fmt.Errorf("cannot find club %s", code)
	}

	name = club.Name + " [" + code + "]"
	adv = indent + name
	if !address {
		return adv, nil
	}
	lines := strings.SplitN(club.Address, "\n", -1)
	adv = indent + name
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		adv += "\n" + indent + line
	}
	return adv, nil

}
