BINDIR := ${GOPATH}/bin
ROOT := $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
PROG := $(notdir ${ROOT})
result1 := $(shell rm -rf ${ROOT}/config)
result2 := $(shell pkl-gen-go pkl/App.pkl --base-path trypkl)

all:  mod vet fmt build file

test:
	cd ${ROOT} && go test ./...

vet:
	cd ${ROOT} && go vet ./...

fmt:
	cd ${ROOT} && go list -f '{{.Dir}}' ./... | grep -v /vendor/ | xargs -L1 gofmt -l

mod:
	cd ${ROOT} && go mod tidy && go mod vendor

build:
	cd ${ROOT} && go build


install:
	cd ${ROOT} && go install -v ./...

file:
	cd ${ROOT} && mv ${PROG} ${BINDIR} && chmod +x ${BINDIR}/${PROG}

